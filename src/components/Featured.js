import React from 'react';
import { Carousel } from 'react-bootstrap';

import image1 from './Images/i1.jpg';
import image2 from './Images/i2.jpg';
import image3 from './Images/i3.jpg';

const Featured = () => {
  return (
    <Carousel fade={true} pause={false}>
      <Carousel.Item interval={5000}>
        <img
          className="d-block w-100"
          style= {{height: 500}}
          src={image1}
          alt="First slide"
        />
        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={5000}>
        <img
          className="d-block w-100"
          style= {{height: 500}}
          src={image2}
          alt="Third slide"
        />
        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={5000}>
        <img
          className="d-block w-100"
          style= {{height: 500}}
          src={image3}
          alt="Third slide"
        />
        <Carousel.Caption>
         
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  )
}

      

export default Featured;