import { useContext } from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { FiShoppingCart } from "react-icons/fi";
import{Card, Dropdown, Badge, Button} from 'react-bootstrap';
import AppContext from '../AppContext'
import pic from './Images/LOGO.jpg'



export default function NavBar() {

	const {user} = useContext(AppContext);


	return(
	<>
			<Navbar bg="dark" expand="lg" className=" navMain pt-4 pb-4">

			    <Container>
			       <Link to="/">
                     <img src={pic} alt = '' height={50} width={200} />
                   </Link>
			        <form className="container-fluid">
			            <div className="input-group">
			              <input type="text" className="form-control me-2" aria-describedby="basic-addon1"/>
			              <button className="btn btn-outline-light" type="submit">Search</button>
			            </div>
			    	</form>
			    	<Nav>
			    		{user.isAdmin
			            ?
			            <Nav.Link as={ Link } to="/adminview" className="text-light">Admin Dashboard</Nav.Link>
			            :
			            <Nav.Link as={ Link } to="/adminview" hidden className="text-light">Admin Dashboard</Nav.Link>
		            	}
		            	{(user.id !== null && user.isAdmin !== true)
		            	?
		            	<Button variant="dark" as={ Link } to= {`/Cart/${user.id}`}>
			    				<FiShoppingCart size={30}/>	
			    		</Button>
		            	:
		            	<Button variant="dark" as={ Link } to={`/Cart/${user.id}`} hidden>
			    				<FiShoppingCart size={30}/>	
			    		</Button>
		           		 }

			    	    {(user.id !== null)
			    	    	?
			    	    	<>
			    	    	<Nav.Link as={ Link } to="/logout" className="text-light">Logout</Nav.Link>
			    	    	</>
			    	    	:
			    	    	<>
			    	    	<Nav.Link as={ Link } to="/login" className="text-light">Login</Nav.Link>
    			            <Nav.Link as={ Link } to="/signup" className="text-light">Signup</Nav.Link>
    			            </>			    	    			    	      
			    	    }
						<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="dark"></Dropdown.Toggle>

			    			<Dropdown.Menu>
							<Nav.Link as={Link} to="/productview"  className= "me-2 text-dark" bg = "dark" >
			    			Home
			    			</Nav.Link>
							
							<Nav.Link as={Link} to="/"  className= "me-2 text-dark" >
			    			Products
			    			</Nav.Link>
			    				
			    			</Dropdown.Menu>
			    		</Dropdown>				    	    		 
			    	</Nav>
			    </Container>
			</Navbar>

			
	</>
	)
}




